<?php require_once('init.php'); ?>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <h2>You are: <?php print $_GET['ps_username']; ?></h2>
        <form class='details'>
            <p>
                I am: 
                <select id='status'>
                    <option value='OFFLINE'>Offline</option>
                    <option value='AVAILABLE'>Available</option>
                    <option value='AWAY'>Away</option>
                    <option value='BUSY'>Busy</option>
                </select>
                and I'm feeling:
                <input type='text' id='status-message'>
                <input type='submit' value='Update'>
            </p>
        </form>
    </body>
</html>

<!-- libraries -->
<script type='text/javascript' src='http://192.168.250.15:8080/gateway/adapter.js'></script>
<script type='text/javascript' src='http://192.168.250.15:8080/gateway/fusion-client-sdk.js'></script>

<!-- Although not required by the FCSDK, jQuery will ease interactions with the DOM -->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

<!-- setup Fusion Client SDK --> 
<script type='text/javascript'>

    // make the UI inactive until the UC is initialised
    $('input, select').attr('disabled', 'disabled');
    
    // Client SDK init code
    var sessionID = "<?= $_SESSION['sessionid'] ?>";

    UC.start(sessionID, []);

    UC.onInitialised = function () {
        $('input').removeAttr('disabled');
    };

    // setup the presence handlers
    UC.presence.onOwnStatusChange = function (status, statusMessage) {
        // in case the select is disabled, enable it
        $('select').removeAttr('disabled');
        
        // update the <select id='#status'> with the value of the
        // status you've discovered through this function
        $('#status').val(status);

        // update the <input type='text' id='status-message'> with 
        // the value of the statusMessage you've discovered through this function
        // status message is optional - so check before updating UI
        if (statusMessage) {
            $('#status-message').text(statusMessage);
        }

        // if you're currently 'OFFLINE', ask the user if they want
        // to appear online
        if (status === 'OFFLINE' && confirm ('Offline - want to go online?')) {
            UC.presence.setStatus('AVAILABLE', statusMessage);
        }
    };
    
    // when the user updates their details
    $('form.details').submit(function (e) {
        e.preventDefault();
        // retrieve the users' details and use the 
        // UC.presence object to update their status 
        // on the presence server
        var status = $('#status').val();
        var statusMessage = $('#status-message').val();
        UC.presence.setStatus(status, statusMessage);
    });
</script>
